﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PracowniaProgramowania_Przyrost2.Serialization
{
    interface ISerializator
    {
        bool SaveToFile<T>(T[] array, string filename);
        T[] DeserializeToObject<T>(string fileName);
    }
}
