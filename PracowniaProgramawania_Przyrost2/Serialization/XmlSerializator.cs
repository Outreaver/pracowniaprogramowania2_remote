﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Serialization
{
    public class XmlSerializator : ISerializator
    {
        public bool SaveToFile<T>(T[] array, string filename)
        {
            if (array.Length == 0)
                return false;
            try
            {

                var xmlSerializer = new XmlSerializer(typeof(T[]));
                using (var file = File.Create($"{filename}.xml"))
                {
                    xmlSerializer.Serialize(file, array);
                }
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not serialize to xml. {ex.Message}");
                return false;
            }
        }

        public T[] DeserializeToObject<T>(string fileName)
        {
            try
            {
                var fileText = File.ReadAllText($"{fileName}.xml");
                var xmlSerializer = new XmlSerializer(typeof(T[]));
                using (var reader = new StringReader(fileText))
                {
                    var result = (T[])xmlSerializer.Deserialize(reader);
                    foreach(var element in result)
                    {
                        var properties = typeof(T).GetProperties();
                        foreach (var property in properties)
                        {
                            var customAttributes = property.CustomAttributes.ToArray();
                            if (customAttributes.Select(x => x.AttributeType).ToArray().Contains(typeof(XmlIgnoreAttribute)))
                                continue;
                            if (property.GetValue(element).ToString() == string.Empty)
                                return null;
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not convert xml to object. {ex.Message}");
                return null;
            }
        }
    }
}
