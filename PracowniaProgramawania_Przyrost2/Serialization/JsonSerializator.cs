﻿using System;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Linq;

namespace PracowniaProgramowania_Przyrost2.Serialization
{
    public class JsonSerializator : ISerializator
    {
        public bool SaveToFile<T>(T[] array, string filename)
        {
            if (array.Length == 0)
                return false;
            try
            {
                var serializerSettings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    Formatting = Formatting.Indented,
                    Converters = new JsonConverter[] { new StringEnumConverter() }
                };

                var json = JsonConvert.SerializeObject(array, serializerSettings);
                File.WriteAllText($"{filename}.json", json);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not serialize to json. {ex.Message}");
                return false;
            }
        }

        public T[] DeserializeToObject<T>(string fileName)
        {
            try
            {
                var fileText = File.ReadAllText($"{fileName}.json");
                var serializerSettings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    Formatting = Formatting.Indented,
                    Converters = new JsonConverter[] { new StringEnumConverter() }
                };
                var json = JsonConvert.DeserializeObject<T[]>(fileText, serializerSettings);
                foreach (var element in json)
                {
                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        var customAttributes = property.CustomAttributes.ToArray();
                        if (customAttributes.Select(x => x.AttributeType).ToArray().Contains(typeof(JsonIgnoreAttribute)))
                            continue;
                        if (property.GetValue(element) == null || property.GetValue(element).ToString() == string.Empty)
                            return null;
                    }
                }
                return json;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Could not convert json to object. {ex.Message}");
                return new T[0];
            }
        }
    }
}
