﻿using System;
using System.Collections.Generic;
using System.Text;
using PracowniaProgramowania_Przyrost2.Models;

namespace PracowniaProgramowania_Przyrost2.Queries
{
    public class AddDataToDatabase
    {
        private pracProg_s416305Context _context;

        public AddDataToDatabase(pracProg_s416305Context context)
        {
            _context = context;
        }

        public bool AddGrade(Grades[] grades)
        {
            try
            {
                _context.Grades.AddRange(grades);
                var savedRecords = _context.SaveChanges();
                return savedRecords > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
