﻿using System;
using System.Collections.Generic;
using System.Linq;
using PracowniaProgramowania_Przyrost2.Models;
using System.Text;
using PracowniaProgramowania_Przyrost2.Models.SeriazlizableModels;

namespace PracowniaProgramowania_Przyrost2.Queries
{
    public class DataBaseQueries
    {
        private pracProg_s416305Context _context;

        public DataBaseQueries(pracProg_s416305Context context)
        {
            _context = context;
        }

        public ClassAndEducator[] GetClassesAndEducators()
        {
            return (from classes in _context.Classes
                    join teachers in _context.Teachers on classes.Educator equals teachers.TeacherId
                    select new ClassAndEducator { Class = classes.Name, EducatorName = teachers.Name, EducatorSurname = teachers.Surname }).ToArray();
        }
        public TeacherAndClassroom[] GetTeachersAndClassromms()
        {
            return (from classromms in _context.Classrooms
                    join teachers in _context.Teachers on classromms.ClassroomId equals teachers.Classroom
                    select new TeacherAndClassroom { TeacherName = teachers.Name, TeacherSurname = teachers.Surname, Classroom = classromms.Name }).ToArray();
        }

        public Students[] GetClassStudents(string className)
        {
            var data = (from classes in _context.Classes
                        join students in _context.Students on classes.ClassId equals students.Class
                        where classes.Name == className
                        select new { students.Pesel, students.Name, students.Surname, students.Gender, students.Adress, students.Class }).ToArray();

            var studentsList = new List<Students>();
            foreach (var student in data)
            {
                studentsList.Add(new Students
                {
                    Pesel = student.Pesel,
                    Name = student.Name,
                    Surname = student.Surname,
                    Gender = student.Gender,
                    Adress = student.Adress,
                    Class = student.Class
                });
            }
            return studentsList.ToArray();
        }

        public GradeAndSubject[] GetStudentGrades(string name, string surname)
        {
            return (from grades in _context.Grades
                    join students in _context.Students on grades.Student equals students.StudentId
                    join teacherClasses in _context.TeachersClasses on grades.Teacher equals teacherClasses.Id
                    join teachers in _context.Teachers on teacherClasses.Teacher equals teachers.TeacherId
                    where students.Name == name && students.Surname == surname
                    select new GradeAndSubject
                    {
                        Grade = grades.Grade,
                        Weight = grades.Weight,
                        Subject = teacherClasses.Subject,
                        TeacherName = teachers.Name,
                        TeacherSurname = teachers.Surname,
                        IssueDate = grades.IssueDate
                    }).ToArray();
        }

        public GradeAndSubject[] GetStudentGradeFromSubject(string name, string surname, string subject)
        {
            return (from grades in _context.Grades
                    join students in _context.Students on grades.Student equals students.StudentId
                    join teacherClasses in _context.TeachersClasses on grades.Teacher equals teacherClasses.Id
                    join teachers in _context.Teachers on teacherClasses.Teacher equals teachers.TeacherId
                    where students.Name == name && students.Surname == surname && teacherClasses.Subject == subject
                    select new GradeAndSubject
                    {
                        Grade = grades.Grade,
                        Weight = grades.Weight,
                        Subject = teacherClasses.Subject,
                        TeacherName = teachers.Name,
                        TeacherSurname = teachers.Surname,
                        IssueDate = grades.IssueDate
                    }).ToArray();
        }
    }
}
