﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class pracProg_s416305Context : DbContext
    {
        public pracProg_s416305Context()
        {
        }

        public pracProg_s416305Context(DbContextOptions<pracProg_s416305Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Classes> Classes { get; set; }
        public virtual DbSet<Classrooms> Classrooms { get; set; }
        public virtual DbSet<Grades> Grades { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<Teachers> Teachers { get; set; }
        public virtual DbSet<TeachersClasses> TeachersClasses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=mssql-2017.labs.wmi.amu.edu.pl;Initial Catalog=pracProg_s416305;User ID=s416305;Password=iBQqIN1i8b;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Classes>(entity =>
            {
                entity.HasKey(e => e.ClassId)
                    .HasName("PK__Classes__7577347E1771C8BD");

                entity.HasIndex(e => e.Educator)
                    .HasName("UQ__Classes__7FB3CA9764A2CD20")
                    .IsUnique();

                entity.HasIndex(e => e.Name)
                    .HasName("UQ__Classes__72E12F1BE5965BF8")
                    .IsUnique();

                entity.Property(e => e.ClassId).HasColumnName("classId");

                entity.Property(e => e.Educator).HasColumnName("educator");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(20);

                entity.HasOne(d => d.EducatorNavigation)
                    .WithOne(p => p.Classes)
                    .HasForeignKey<Classes>(d => d.Educator)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Classes__educato__66603565");
            });

            modelBuilder.Entity<Classrooms>(entity =>
            {
                entity.HasKey(e => e.ClassroomId)
                    .HasName("PK__Classroo__22E187ACE9466AC9");

                entity.HasIndex(e => e.Name)
                    .HasName("UQ__Classroo__72E12F1BBF473FAD")
                    .IsUnique();

                entity.Property(e => e.ClassroomId).HasColumnName("classroomId");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Grades>(entity =>
            {
                entity.HasKey(e => e.GradeId)
                    .HasName("PK__Grades__FB4362F9B377E750");

                entity.Property(e => e.GradeId).HasColumnName("gradeId");

                entity.Property(e => e.Grade)
                    .IsRequired()
                    .HasColumnName("grade")
                    .HasMaxLength(5);

                entity.Property(e => e.IssueDate)
                    .HasColumnName("issueDate")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Student).HasColumnName("student");

                entity.Property(e => e.Teacher).HasColumnName("teacher");

                entity.Property(e => e.Weight).HasColumnName("weight");

                entity.HasOne(d => d.StudentNavigation)
                    .WithMany(p => p.Grades)
                    .HasForeignKey(d => d.Student)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Grades__student__6754599E");

                entity.HasOne(d => d.TeacherNavigation)
                    .WithMany(p => p.Grades)
                    .HasForeignKey(d => d.Teacher)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Grades__teacher__68487DD7");
            });

            modelBuilder.Entity<Students>(entity =>
            {
                entity.HasKey(e => e.StudentId)
                    .HasName("PK__Students__4D11D63C17AA4C8A");

                entity.HasIndex(e => e.Pesel)
                    .HasName("UQ__Students__DC3B1BB8422F7840")
                    .IsUnique();

                entity.Property(e => e.StudentId).HasColumnName("studentId");

                entity.Property(e => e.Adress)
                    .HasColumnName("adress")
                    .HasMaxLength(50);

                entity.Property(e => e.Class).HasColumnName("class");

                entity.Property(e => e.Gender)
                    .IsRequired()
                    .HasColumnName("gender")
                    .HasMaxLength(1);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Pesel)
                    .HasColumnName("pesel")
                    .HasMaxLength(11);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasColumnName("surname")
                    .HasMaxLength(50);

                entity.HasOne(d => d.ClassNavigation)
                    .WithMany(p => p.Students)
                    .HasForeignKey(d => d.Class)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Students__class__693CA210");
            });

            modelBuilder.Entity<Teachers>(entity =>
            {
                entity.HasKey(e => e.TeacherId)
                    .HasName("PK__Teachers__98E938959957CE41");

                entity.Property(e => e.TeacherId).HasColumnName("teacherId");

                entity.Property(e => e.Classroom).HasColumnName("classroom");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Surname)
                    .HasColumnName("surname")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.ClassroomNavigation)
                    .WithOne(p => p.Teachers)
                    .HasForeignKey<Teachers>(d => d.Classroom)
                    .HasConstraintName("FK__Teachers__classr__6A30C649");
            });

            modelBuilder.Entity<TeachersClasses>(entity =>
            {
                entity.ToTable("Teachers_Classes");

                entity.HasIndex(e => new { e.Class, e.Teacher, e.Subject })
                    .HasName("UQ__Teachers__4E2250A86F646D19")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Class).HasColumnName("class");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasColumnName("subject")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Teacher).HasColumnName("teacher");

                entity.HasOne(d => d.ClassNavigation)
                    .WithMany(p => p.TeachersClasses)
                    .HasForeignKey(d => d.Class)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Teachers___class__6B24EA82");

                entity.HasOne(d => d.TeacherNavigation)
                    .WithMany(p => p.TeachersClasses)
                    .HasForeignKey(d => d.Teacher)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Teachers___teach__6C190EBB");
            });
        }
    }
}
