﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models.SeriazlizableModels
{
    public class ClassAndEducator
    {
        [JsonProperty]
        [XmlElement]
        public string Class {get; set; }
        [JsonProperty]
        [XmlElement]
        public string EducatorName { get; set; }
        [JsonProperty]
        [XmlElement]
        public string EducatorSurname { get; set; }
    }
}
