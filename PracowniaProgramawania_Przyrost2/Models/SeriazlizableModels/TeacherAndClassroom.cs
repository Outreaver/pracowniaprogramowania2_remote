﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models.SeriazlizableModels
{
    public class TeacherAndClassroom
    {
        [JsonProperty]
        [XmlElement]
        public string TeacherName { get; set; }
        [JsonProperty]
        [XmlElement]
        public string TeacherSurname { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Classroom { get; set; }
    }
}
