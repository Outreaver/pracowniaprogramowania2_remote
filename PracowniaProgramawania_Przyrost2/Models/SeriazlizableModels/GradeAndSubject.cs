﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models.SeriazlizableModels
{
    public class GradeAndSubject
    {
        [JsonProperty]
        [XmlElement]
        public string Grade { get; set; }
        [JsonProperty]
        [XmlElement]
        public int Weight { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Subject { get; set; }
        [JsonProperty]
        [XmlElement]
        public string TeacherName { get; set; }
        [JsonProperty]
        [XmlElement]
        public string TeacherSurname { get; set; }
        [JsonProperty]
        [XmlElement]
        public DateTime IssueDate { get; set; }
    }
}
