﻿using System;
using System.Collections.Generic;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class TeachersClasses
    {
        public TeachersClasses()
        {
            Grades = new HashSet<Grades>();
        }

        public int Id { get; set; }
        public int Class { get; set; }
        public int Teacher { get; set; }
        public string Subject { get; set; }

        public virtual Classes ClassNavigation { get; set; }
        public virtual Teachers TeacherNavigation { get; set; }
        public virtual HashSet<Grades> Grades { get; set; }
    }
}
