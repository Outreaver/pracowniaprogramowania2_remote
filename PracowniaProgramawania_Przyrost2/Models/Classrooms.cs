﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class Classrooms
    {
        [JsonProperty]
        [XmlElement]
        public int ClassroomId { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Name { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual Teachers Teachers { get; set; }
    }
}
