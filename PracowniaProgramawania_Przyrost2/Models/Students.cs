﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class Students
    {
        public Students()
        {
            Grades = new HashSet<Grades>();
        }

        [JsonIgnore]
        [XmlIgnore]
        public int StudentId { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Pesel { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Name { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Surname { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Gender { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Adress { get; set; }
        [JsonProperty]
        [XmlElement]
        public int Class { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual Classes ClassNavigation { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual HashSet<Grades> Grades { get; set; }
    }
}
