﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class Teachers
    {
        public Teachers()
        {
            TeachersClasses = new HashSet<TeachersClasses>();
        }

        [JsonIgnore]
        [XmlIgnore]
        public int TeacherId { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Name { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Surname { get; set; }
        [JsonProperty]
        [XmlElement]
        public int? Classroom { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual Classrooms ClassroomNavigation { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual Classes Classes { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual HashSet<TeachersClasses> TeachersClasses { get; set; }
    }
}
