﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class Grades
    {
        [JsonIgnore]
        [XmlIgnore]
        public int GradeId { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Grade { get; set; }
        [JsonProperty]
        [XmlElement]
        public int Weight { get; set; }
        [JsonProperty]
        [XmlElement]
        public int Student { get; set; }
        [JsonProperty]
        [XmlElement]
        public int Teacher { get; set; }
        [JsonProperty]
        [XmlElement]
        public DateTime IssueDate { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual Students StudentNavigation { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual TeachersClasses TeacherNavigation { get; set; }
    }
}
