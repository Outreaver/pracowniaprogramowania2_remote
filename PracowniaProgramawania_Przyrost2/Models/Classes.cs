﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace PracowniaProgramowania_Przyrost2.Models
{
    public partial class Classes
    {
        public Classes()
        {
            Students = new HashSet<Students>();
            TeachersClasses = new HashSet<TeachersClasses>();
        }

        [JsonIgnore]
        [XmlIgnore]
        public int ClassId { get; set; }
        [JsonProperty]
        [XmlElement]
        public string Name { get; set; }
        [JsonProperty]
        [XmlElement]
        public int Educator { get; set; }

        [JsonIgnore]
        [XmlIgnore]
        public virtual Teachers EducatorNavigation { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual HashSet<Students> Students { get; set; }
        [JsonIgnore]
        [XmlIgnore]
        public virtual HashSet<TeachersClasses> TeachersClasses { get; set; }
    }
}
