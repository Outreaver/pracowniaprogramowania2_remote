using PracowniaProgramowania_Przyrost2.Models;
using PracowniaProgramowania_Przyrost2.Queries;
using PracowniaProgramowania_Przyrost2.Serialization;
using System;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class JsonTest
    {
        private pracProg_s416305Context _context;
        private DataBaseQueries _databaseDataGetter;
        private AddDataToDatabase _addDataToDatabase;
        private JsonSerializator _jsonSerializator;

        public JsonTest()
        {
            _context = new pracProg_s416305Context();
            _databaseDataGetter = new DataBaseQueries(_context);
            _addDataToDatabase = new AddDataToDatabase(_context);
            _jsonSerializator = new JsonSerializator();
        }

        [Fact]
        public void JsonSerializationTest()
        {
            var correctData = new List<object[]>
            {
                _databaseDataGetter.GetTeachersAndClassromms(),
                _databaseDataGetter.GetClassesAndEducators(),
                _databaseDataGetter.GetClassStudents("2A"),
                _databaseDataGetter.GetStudentGrades("Aleksandra", "Kica�o"),
                _databaseDataGetter.GetStudentGradeFromSubject("Aleksandra", "Kica�o", "Matematyka")
            };

            var i = 0;
            foreach (var element in correctData)
            {
                ++i;
                Assert.True(_jsonSerializator.SaveToFile(element, $"result-{i}"));
            }

            var wrongData = new List<object[]>
            {
                _databaseDataGetter.GetClassStudents("kaszanka"),
                _databaseDataGetter.GetStudentGrades("aaa", "vvv"),
                _databaseDataGetter.GetStudentGradeFromSubject("asd", "weqq", "qwe")
            };

            foreach (var element in wrongData)
            {
                Assert.False(_jsonSerializator.SaveToFile(element, "wrong"));
            }
        }
        
        [Fact]
        public void JsonDeserializationTest()
        {
            var data = _jsonSerializator.DeserializeToObject<Grades>("gradesCorrect");
            Assert.True(_addDataToDatabase.AddGrade(data));
            data = _jsonSerializator.DeserializeToObject<Grades>("gradesWrong");
            Assert.False(_addDataToDatabase.AddGrade(data));
        }
    }
}
