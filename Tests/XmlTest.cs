﻿using PracowniaProgramowania_Przyrost2.Models;
using PracowniaProgramowania_Przyrost2.Models.SeriazlizableModels;
using PracowniaProgramowania_Przyrost2.Queries;
using PracowniaProgramowania_Przyrost2.Serialization;
using System;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public class XmlTest
    {
        private static int i = 0;
        private pracProg_s416305Context _context;
        private DataBaseQueries _databaseDataGetter;
        private AddDataToDatabase _addDataToDatabase;
        private XmlSerializator _xmlSerializator;

        public XmlTest()
        {
            _context = new pracProg_s416305Context();
            _databaseDataGetter = new DataBaseQueries(_context);
            _addDataToDatabase = new AddDataToDatabase(_context);
            _xmlSerializator = new XmlSerializator();
        }

        [Fact]
        public void XmlSerializationTest()
        {
            var data = new Dictionary<string, (object[] good, object[] wrong)>
            {
                {"TeacherAndClass", ( _databaseDataGetter.GetTeachersAndClassromms(), null )},
                {"ClassesEndEducators", ( _databaseDataGetter.GetClassesAndEducators(), null)},
                {"ClassStudents", ( _databaseDataGetter.GetClassStudents("2A"), _databaseDataGetter.GetClassStudents("kaszanka"))},
                {"StudentGrade", ( _databaseDataGetter.GetStudentGrades("Aleksandra", "Kicało"), _databaseDataGetter.GetStudentGrades("aaa", "vvv"))},
                {"StudentGradeFromSubject", ( _databaseDataGetter.GetStudentGradeFromSubject("Aleksandra", "Kicało", "Matematyka"), _databaseDataGetter.GetStudentGradeFromSubject("asd", "weqq", "qwe")) }
            };

            DoTest((TeacherAndClassroom[])data["TeacherAndClass"].good,(TeacherAndClassroom[]) data["TeacherAndClass"].wrong);
            DoTest((ClassAndEducator[])data["ClassesEndEducators"].good, (ClassAndEducator[])data["ClassesEndEducators"].wrong);
            DoTest((Students[])data["ClassStudents"].good, (Students[])data["ClassStudents"].wrong);
            DoTest((GradeAndSubject[])data["StudentGrade"].good, (GradeAndSubject[])data["StudentGrade"].wrong);
            DoTest((GradeAndSubject[])data["StudentGradeFromSubject"].good, (GradeAndSubject[])data["StudentGradeFromSubject"].wrong);
        }
        
        [Fact]
        public void XmlDeserializationTest()
        {
            var data = _xmlSerializator.DeserializeToObject<Grades>("gradesCorrect");
            Assert.True(_addDataToDatabase.AddGrade(data));
            data = _xmlSerializator.DeserializeToObject<Grades>("gradesWrong");
            Assert.False(_addDataToDatabase.AddGrade(data));
        }
        
        private void DoTest<T>(T[] goodData, T[] wrongData)
        {
            i++;
            Assert.True(_xmlSerializator.SaveToFile<T>(goodData, $"result-{i}"));
            if (wrongData != null)
                Assert.False(_xmlSerializator.SaveToFile<T>(wrongData, string.Empty));
        }
    }
}
